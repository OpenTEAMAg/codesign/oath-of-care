![image](https://gitlab.com/OpenTEAMAg/codesign/oath-of-care/-/raw/master/OathofCareLogoWide-09.png)

<i> This page contains the version 1 realease of the Oath of Care. View the latest version and other Ag Data USe Agreement Documents here: <a href="https://openteam-agreements.community/">openteam-agreements.community</a> </i>

<h2 style="text-align: center;">Preamble</h2>

<p>This oath means to protect the collaborative integrity of land stewards and advisors.  It is meant to uphold the intellectual property of land stewards and support a commitment to the long term collaborative well being of the land we all rely on.</p><br>

<p>We believe that farmers, ranchers and other land stewards have a right to know that their advisor is acting on their behalf to represent their best interests. For those land stewards, the following is an oath declaring an advisor’s commitment to adhere to a fiduciary ethic, act in good faith, and maintain a signed copy of this oath when they enter into an advisory relationship with a client. Similarly, we recommend that land stewards insist the oath be signed by their advisors before entering into a relationship.</p><br>


<p>Advising from agricultural professionals impacts a system that affects not just the health of a single client but that of all life and our shared knowledge about the natural world which supports us. Consequently, we present a set of easy-to-remember principles that are easy to teach, and easy to hold people accountable to, just like principles taught to doctors. </p><br>


<p>In its original form, the Hippocratic Oath requires a new physician to swear to uphold specific ethical standards. Of historic and traditional value, the oath is considered a rite of passage for practitioners of medicine. But why only physicians? Why not holders of public data, why not engineers, why not scientists, why not businesses, and why not agriculturalists? What about the care of the data about the health and well-being of our public atmosphere, our soils, our water, and our data about how the system of our biosphere functions and how we can create health or destroy it? It seems time that we extend to the agricultural profession a version of the physician’s and the fiduciary oath and to treat land stewards and the data about the land with the same care and confidence that a doctor would their patient.*</p> 

<h2 style="text-align: center;">Data Fiduciary Oath of Care for Agricultural Professionals</h2>


<p style="text-align: center;"><strong>Putting the Land Steward’s interests first</strong></p>
<br>
I shall put the needs and interests of the land and the land steward ahead of my own. Therefore, I am proud to commit to the following five fiduciary principles:

  1. I will always put the land steward’s best interests first, and help, to the best of my ability,  to navigate short and long term tradeoffs.
  2. I will act with prudence; that is, with the skill, care, diligence, and good judgment of a professional.
  3. I will not mislead. I will provide the whole story and make transparent, full, and fair disclosure of all important facts.
  4. I will avoid conflicts of interest, and should such situations arise, I will immediately disclose the nature of the conflict.
  5. If unavoidable conflicts occur, I will fairly manage them, in the land stewards favor. 

Furthermore I swear to fulfill, to the best of my ability and judgment, this covenant: 

  - I will respect the hard-won scientific gains of those agricultural professionals in whose steps I walk, and gladly share such knowledge as is mine with those who are to follow.
  - I will apply all measures that are required to not sacrifice long-term outcomes for short-term results.
  - I will remember that there is art to agriculture as well as science, and that planning, observation, analysis, and collaboration with others may be the first path before taking action.  
  - I will not give the people who use my recommendations false comfort about their accuracy. Instead, I will make explicit my assumptions and potential oversights.  
  - I will not be ashamed to say “I don’t know” nor will I fail to call in my colleagues when the skills of another are needed. 
  - I understand that my work may have enormous effects on society and the economy, many of them beyond my comprehension, and I look for equitable outcomes and accessible processes for the well-being of humanity and land.
  - I will respect the privacy of land stewards and their operations, for their problems are not disclosed to me that the world may know. 
  - I agree to use my best judgment and knowledge to guide and balance technology decisions that uphold the land steward’s rights. This includes but is not limited to the rights to: erase their data; be informed of changes to agreements that affect them; restrict unauthorized access to their data; preserve access to their own records; preserve the ability to object or move to another fiduciary data advisor without prejudice; and access to data portability.
  - I will remember that I remain a member of society, with obligations to all my fellow human beings to improve the most basic regenerative systems that support us all—those in cities and towns, and those in the country, and those of differing abilities.
  - If and when I find myself unable to carry out these duties, I agree to pass along my accumulated knowledge to those who are willing and eager to uphold this oath.
  - If I do not violate this oath, may I be respected while I live and remembered with affection thereafter. May I always act so as to preserve the finest traditions of my calling and enjoy helping those who seek my council.
<br>

<p>*Adapted from Dr. Louis Lasagna’s 1964 Hippocratic Oath and the Fiduciary Oath used by financial advisors.</p>
<br>