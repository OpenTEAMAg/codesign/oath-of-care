## Versioning Process

This repository maintains the review and versioning process for the OpenTEAM Ag Data Oath of Care. Our most current iteration will be deployed on a static site for review. There will be a surveystack form embedded on that same site for feedback and review. 

Our versioning process will begin on a 6-month interval for the duration of 2023. Starting in 2024, we will have an annual review and versioning process. Request for feedback will be announced with ample time for review before the next versioning process in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter. The feedback and versioning timeline will also be detailed on the OpenTEAM Ag Data Oath of Care static site.

## HTML webpage information

- site_name: OpenTEAM Ag Data Oath of Care
- site_url: https://openteamag.gitlab.io/codesign/oath-of-care/

Now you can add markdown files to your repository and the index will match the folder structure of your files!

